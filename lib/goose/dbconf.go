package goose

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/kylelemons/go-gypsy/yaml"
	"github.com/lib/pq"
)

// DBDriver encapsulates the info needed to work with
// a specific database driver
type DBDriver struct {
	Name    string
	OpenStr string
	Import  string
	Dialect SqlDialect
}

type DBConf struct {
	MigrationsDir string
	Env           string
	Driver        DBDriver
	PgSchema      string
}

// NewDBConfFromEnv - reads config information directly from env variables
// GOOSE_DRIVER, GOOSE_DIALECT, GOOSE_OPEN, GOOSE_IMPORT may be specified
func NewDBConfFromEnv(p, env, pgschema string) (*DBConf, error) {
	drv := os.Getenv("GOOSE_DRIVER")
	drv = os.ExpandEnv(drv)

	dialect := os.Getenv("GOOSE_DIALECT")
	dialect = os.ExpandEnv(dialect)

	open := os.Getenv("GOOSE_OPEN")
	open = os.ExpandEnv(open)

	imprt := os.Getenv("GOOSE_IMPORT")
	imprt = os.ExpandEnv(imprt)

	if drv != "" && open != "" {
		return makeDBConf(dbConfParams{
			Path:     p,
			Env:      env,
			PgSchema: pgschema,
			Driver:   drv,
			Open:     open,
			Import:   imprt,
			Dialect:  dialect,
		})
	} else {
		return nil, fmt.Errorf("GOOSE_DRIVER and GOOSE_OPEN not defined")
	}
}

// extract configuration details from the given file
func NewDBConf(p, env string, pgschema string) (*DBConf, error) {

	cfgFile := filepath.Join(p, "dbconf.yml")

	f, err := yaml.ReadFile(cfgFile)
	if err != nil {
		return nil, err
	}

	drv, err := f.Get(fmt.Sprintf("%s.driver", env))
	if err != nil {
		return nil, err
	}
	drv = os.ExpandEnv(drv)

	open, err := f.Get(fmt.Sprintf("%s.open", env))
	if err != nil {
		return nil, err
	}
	open = os.ExpandEnv(open)

	imprt, err := f.Get(fmt.Sprintf("%s.import", env))
	if err != nil {
		imprt = ""
	}
	imprt = os.ExpandEnv(imprt)

	dialect, err := f.Get(fmt.Sprintf("%s.dialect", env))
	if err != nil {
		dialect = ""
	}
	dialect = os.ExpandEnv(dialect)

	return makeDBConf(dbConfParams{
		Path:     p,
		Env:      env,
		PgSchema: pgschema,
		Driver:   drv,
		Open:     open,
		Import:   imprt,
		Dialect:  dialect,
	})
}

// dbConfParams - holds parameters to make dbconf instances
type dbConfParams struct {
	Path     string
	Env      string
	PgSchema string
	Driver   string
	Open     string
	Import   string
	Dialect  string
}

// makeDBConf - centralized location to create dbconf instance
func makeDBConf(params dbConfParams) (*DBConf, error) {

	// Automatically parse postgres urls
	if params.Driver == "postgres" {

		// Assumption: If we can parse the URL, we should
		if parsedURL, err := pq.ParseURL(params.Open); err == nil && parsedURL != "" {
			params.Open = parsedURL
		}
	}

	d := newDBDriver(params.Driver, params.Open)

	// allow the configuration to override the Import for this driver
	if params.Import != "" {
		d.Import = params.Import
	}

	// allow the configuration to override the Dialect for this driver
	if params.Dialect != "" {
		d.Dialect = dialectByName(params.Dialect)
	}

	if !d.IsValid() {
		return nil, errors.New(fmt.Sprintf("Invalid DBConf: %v", d))
	}

	return &DBConf{
		MigrationsDir: filepath.Join(params.Path, "migrations"),
		Env:           params.Env,
		Driver:        d,
		PgSchema:      params.PgSchema,
	}, nil
}

// Create a new DBDriver and populate driver specific
// fields for drivers that we know about.
// Further customization may be done in NewDBConf
func newDBDriver(name, open string) DBDriver {

	d := DBDriver{
		Name:    name,
		OpenStr: open,
	}

	switch name {
	case "postgres":
		d.Import = "github.com/lib/pq"
		d.Dialect = &PostgresDialect{}

	case "mymysql":
		d.Import = "github.com/ziutek/mymysql/godrv"
		d.Dialect = &MySqlDialect{}

	case "mysql":
		d.Import = "github.com/go-sql-driver/mysql"
		d.Dialect = &MySqlDialect{}

	case "sqlite3":
		d.Import = "github.com/mattn/go-sqlite3"
		d.Dialect = &Sqlite3Dialect{}
	}

	return d
}

// ensure we have enough info about this driver
func (drv *DBDriver) IsValid() bool {
	return len(drv.Import) > 0 && drv.Dialect != nil
}

// OpenDBFromDBConf wraps database/sql.DB.Open() and configures
// the newly opened DB based on the given DBConf.
//
// Callers must Close() the returned DB.
func OpenDBFromDBConf(conf *DBConf) (*sql.DB, error) {
	db, err := sql.Open(conf.Driver.Name, conf.Driver.OpenStr)
	if err != nil {
		return nil, err
	}

	// if a postgres schema has been specified, apply it
	if conf.Driver.Name == "postgres" && conf.PgSchema != "" {
		if _, err := db.Exec("SET search_path TO " + conf.PgSchema); err != nil {
			return nil, err
		}
	}

	return db, nil
}
